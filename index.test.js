const assert = require('chai').assert;

describe('Aplicación web', () => {
  it('La página debe contener un título', () => {
    const pageTitle = document.title;
    assert.equal(pageTitle, 'Aplicación Web Simple');
  });

  it('El título debe ser "Hola, esta es mi aplicación web simple."', () => {
    const h1Text = document.querySelector('h1').textContent;
    assert.include(h1Text, 'Hola, esta es mi aplicación web simple.');
  });
});
